#!/bin/sh

ARENA_CLASS=ntu.csie.oop13spring.MyArena
PET1_CLASS=ntu.csie.oop13spring.Android
PET2_CLASS=ntu.csie.oop13spring.Pikachu
PET3_CLASS=ntu.csie.oop13spring.Mario
PET4_CLASS=ntu.csie.oop13spring.Android
PET5_CLASS=ntu.csie.oop13spring.Android

javac -cp hw4.jar -d bin/ src/*.java
java -cp bin/ -jar bin/hw4.jar $ARENA_CLASS $PET1_CLASS $PET2_CLASS $PET3_CLASS $PET4_CLASS $PET5_CLASS
