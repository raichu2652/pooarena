package ntu.csie.oop13spring;

import javax.swing.*;

public class Android extends POOPetKind {
	public Android() {
		setHP(1000);
		setFullHP(1000);
		setRecoverHP(0);
		setMP(500);
		setFullMP(500);
		setRecoverMP(10);
		setAGI(3);
		setSize(70, 70);
		
		originalAGI = getAGI();
		
		nLabels = 3;
		charLabels = new CharLabel[nLabels];
		charLabels[0] = new CharLabel(70, 70, new ImageIcon("src\\android\\default.png"));
		charLabels[1] = new CharLabel(120, 70, new ImageIcon("src\\android\\flash.png"));
		charLabels[2] = new CharLabel(100, 120, new ImageIcon("src\\android\\hurricane.png"));

		charlabel = charLabels[0];
		System.out.println("Android Created!");
	}
	
	public void setDefault() {
		change(70, 70, 0);
	}
	
    public POOAction act(POOArena arena) {
		POOAction action = new POOAction();

		if (skillLock) { return null; }

		switch(actno) {
			case 1:
				action.skill = new Flash();
				action.dest = this;
				lock();
				return action;
			case 2:
				action.skill = new Hurricane(this);
				action.dest = enemyPet;
				lock();
				return action;
			case 0:
			default: return null;
		}
	}
}