package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

import javax.swing.*;
import java.awt.*;

public class ArrowLabel extends ImageLabel {
	public int height;
	private boolean up, down;

	public ArrowLabel(int width, int height, ImageIcon originalImage) {
		super(width, height, originalImage);
		
		height = 0;
	}

	public void move() {
		if (up && height > 0) {
			setLocation(getLocation().x, getLocation().y - getHeight());
			height--;
		}
		if (down & height < 4) {
			setLocation(getLocation().x, getLocation().y + getHeight());
			height++;
		}

		repaint();
	}
	
	public void setUp(boolean newUp) {
		up = newUp;
	}
	
	public void setDown(boolean newDown) {
		down = newDown;
	}
}