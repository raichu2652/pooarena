package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

import javax.swing.*;
import java.awt.*;

public class Bar extends JPanel {
	int value;
	int full;
	public Color textColor;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int width = getWidth() - 4;
		int height = getHeight() - 4;
		int x = 2;
		int y = 2;
		
		g.setColor(getBackground());
		g.fillRect(x, y, width, height);
		g.setColor(Color.black);
		g.drawRect(x, y, width, height);
		
		g.setColor(getForeground());
		g.fillRect(x, y, (width * value / full), height);
		g.setColor(Color.black);
		g.drawRect(x, y, (width * value / full), height);
		
		g.setColor(textColor);
		g.drawString(Integer.toString(value), width / 2, (height + 10) / 2);
	}
	
	public Dimension getMaximumSize() {
		return getPreferredSize();
	}
}

class HealthBar extends Bar {
	public POOPetKind Pet;
	
	public HealthBar(POOPetKind Pet) {
		setOpaque(false);
		// setSize(new Dimension(230, 30));
		// setMinimumSize(new Dimension(230, 30));
		setPreferredSize(new Dimension(300, 30));
		setForeground(Color.red);
		setBackground(Color.lightGray);
		
		this.Pet = Pet;
		textColor = Color.white;
	}
	
	public void paintComponent(Graphics g) {
		value = Pet.getHP();
		full = Pet.getFullHP();
		
		super.paintComponent(g);
	}
}

class ManaBar extends Bar {
	public POOPetKind Pet;

	public ManaBar(POOPetKind Pet) {
		setOpaque(false);
		// setSize(new Dimension(230, 30));
		// setMinimumSize(new Dimension(230, 30));
		setPreferredSize(new Dimension(300, 30));
		setForeground(Color.blue);
		setBackground(Color.lightGray);
		
		this.Pet = Pet;
		textColor = Color.white;
	}
	
	public void paintComponent(Graphics g) {
		value = Pet.getMP();
		full = Pet.getFullMP();
		
		super.paintComponent(g);
	}
}