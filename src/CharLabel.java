package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

import javax.swing.*;
import java.awt.*;

public class CharLabel extends ImageLabel {
	public CharLabel(int width, int height, ImageIcon originalImage) {
		super(width, height, originalImage);
	}

	public void move2(Point P) {
		setLocation(P);
		setSize(size);

		repaint();
	}
}