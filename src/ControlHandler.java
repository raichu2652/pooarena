package ntu.csie.oop13spring;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ControlHandler extends KeyAdapter {
	POOPetKind p1, p2;
	
	public ControlHandler(Player p1, Player p2) {
		this.p1 = p1.Pet;
		this.p2 = p2.Pet;

		System.out.println("Control handler initialized!");
	}
	
	public void keyTyped(KeyEvent arg0) {}
	
	public void keyReleased(KeyEvent key) {
		switch(key.getKeyCode()) {
			case KeyEvent.VK_W:
				p1.setUp(false);
				break;
			case KeyEvent.VK_S:
				p1.setDown(false);
				break;
			case KeyEvent.VK_A:
				p1.setLeft(false);
				break;
			case KeyEvent.VK_D:
				p1.setRight(false);
			case KeyEvent.VK_UP:
				p2.setUp(false);
				break;
			case KeyEvent.VK_DOWN:
				p2.setDown(false);
				break;
			case KeyEvent.VK_LEFT:
				p2.setLeft(false);
				break;
			case KeyEvent.VK_RIGHT:
				p2.setRight(false);
				break;
			case KeyEvent.VK_CONTROL:
				if (key.getKeyLocation() == KeyEvent.KEY_LOCATION_LEFT)
					p1.setActno(0);
				else
					p2.setActno(0);
				break;
			case KeyEvent.VK_SHIFT:
				if (key.getKeyLocation() == KeyEvent.KEY_LOCATION_LEFT)
					p1.setActno(0);
				else
					p2.setActno(0);
				break;
		}
	}
	
	public void keyPressed(KeyEvent key) {
		switch(key.getKeyCode()) {
			case KeyEvent.VK_W:
				p1.setUp(true);
				break;
			case KeyEvent.VK_S:
				p1.setDown(true);
				break;
			case KeyEvent.VK_A:
				p1.setLeft(true);
				break;
			case KeyEvent.VK_D:
				p1.setRight(true);
				break;
			case KeyEvent.VK_UP:
				p2.setUp(true);
				break;
			case KeyEvent.VK_DOWN:
				p2.setDown(true);
				break;
			case KeyEvent.VK_LEFT:
				p2.setLeft(true);
				break;
			case KeyEvent.VK_RIGHT:
				p2.setRight(true);
				break;
			case KeyEvent.VK_CONTROL:
				if (key.getKeyLocation() == KeyEvent.KEY_LOCATION_LEFT)
					p1.setActno(1);
				else
					p2.setActno(1);
				break;
			case KeyEvent.VK_SHIFT:
				if (key.getKeyLocation() == KeyEvent.KEY_LOCATION_LEFT)
					p1.setActno(2);
				else
					p2.setActno(2);
				break;
		}
	}
}