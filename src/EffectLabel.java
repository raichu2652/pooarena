package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

import javax.swing.*;
import java.awt.*;

public class EffectLabel extends ImageLabel {
	public boolean enabled;
	public int vectorX, vectorY;
	public Point point;
	
	public EffectLabel(int width, int height, ImageIcon originalImage) {
		super(width, height, originalImage);
	}

	public void direct(Point start, Point end, int div) {
		point = new Point(start);
		vectorX = (end.x - start.x) / div;
		vectorY = (end.y - start.y) / div;
	}
	
	public boolean checkBound(int radiusX, int radiusY, POOPetKind enemy) {
		Rectangle effectBound = new Rectangle(this.getLocation().x - radiusX, this.getLocation().y - radiusY, getWidth() + (2 * radiusX), getHeight() + (2 * radiusY));
		
		Point[] Edge = new Point[4];
		Edge[0] = new Point(enemy.getPosition());
		Edge[1] = new Point(enemy.getPosition().x, enemy.getPosition().y + enemy.getHeight());
		Edge[2] = new Point(enemy.getPosition().x + enemy.getWidth(), enemy.getPosition().y);
		Edge[3] = new Point(enemy.getPosition().x + enemy.getWidth(), enemy.getPosition().y + enemy.getHeight());

		if (effectBound.contains(Edge[0]) || effectBound.contains(Edge[1]) || effectBound.contains(Edge[2]) || effectBound.contains(Edge[3]))
			return true;
		else 
			return false;
	}
	
	public void move2(int x, int y) {
		setLocation(x, y);
		setSize(size);
	}
	
	public void move2ward() {
		point.x += vectorX;
		point.y += vectorY;
		setLocation(point);
		setSize(size);
		
		repaint();
	}
}