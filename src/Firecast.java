package ntu.csie.oop13spring;

import javax.swing.*;
import java.awt.*;

public class Firecast extends POOSkill{
	final POOPetKind subject;
	
	public Firecast(POOPetKind Pet) {
		subject = Pet;
	}
	
    public void act(final POOPetKind object) {
		System.out.println("Firecast!");
		
		if (!(subject.setMP(subject.getMP() - 25))) {
			subject.unlock();
			return;
		}
		
		subject.change(140, 120, 1);
		
		Thread timer1 = new Thread(new Runnable() {
			public void run() {
				try {Thread.sleep(150);} catch (InterruptedException e) {e.printStackTrace();}
				subject.change(90, 100, 0);
				subject.unlock();
				
				Point end;
				if (subject.getPosition().x <= object.getPosition().x)
					end = new Point(1000, subject.getPosition().y);
				else
					end = new Point(0, subject.getPosition().y);
				
				int n = -1;
				for (int i = 0; i < 10; i++) {
					if (!subject.effectLabels[i].enabled) {
						subject.effectLabels[i].enabled = true;
						subject.effectLabels[i].setLocation(subject.getPosition());
						subject.effectLabels[i].direct(subject.getPosition(), end, 100);
						subject.effectLabels[i].setVisible(true);
						n = i;
						break;
					}
				}
				if (n < 0) return;

				for (int i = 0; i < 150; i++) {
					if (subject.effectLabels[n].checkBound(0, 0, object)) {
						object.setHP(Math.max(object.getHP() - 70, 0));
						break;
					}
					subject.effectLabels[n].move2ward();
					
					try {Thread.sleep(15);} catch (InterruptedException e) {e.printStackTrace();}
				}
				
				subject.effectLabels[n].enabled = false;
				subject.effectLabels[n].setVisible(false);
				subject.effectLabels[n].move2(1000, 1000);
			}
		});
		timer1.start();
	}
	
	public void act(POOPet target) {
		act((POOPetKind)target);
	}
}