package ntu.csie.oop13spring;

import javax.swing.*;

public class Flash extends POOSkill{
    public void act(final POOPetKind target) {
		System.out.println("Flash!");
		
		if (!(target.setMP(target.getMP() - 100))) {
			target.unlock();
			return;
		}
		
		target.change(120, 70, 1);
		target.setAGI(6);
		
		Thread timer1 = new Thread(new Runnable() {
			public void run() {
				try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
				
				target.change(70, 70, 0);
				target.setAGI(3);
				target.unlock();
			}
		});
		Thread timer2 = new Thread(new Runnable() {
			public void run() {
				try {Thread.sleep(200);} catch (InterruptedException e) {e.printStackTrace();}
				target.unlock();
			}
		});
		timer1.start();
		timer2.start();
	}
	
	public void act(POOPet target) {
		act((POOPetKind)target);
	}
}