package ntu.csie.oop13spring;

import javax.swing.*;

public class Hurricane extends POOSkill{
	final POOPetKind subject;
	
	public Hurricane(POOPetKind Pet) {
		subject = Pet;
	}
	
    public void act(final POOPetKind object) {
		System.out.println("Hurricane!");
		
		if (!(subject.setMP(subject.getMP() - 50))) {
			subject.unlock();
			return;
		}
		
		subject.change(100, 120, 2);
		
		Thread timer1 = new Thread(new Runnable() {
			public void run() {
				for (int i = 0; i < 250; i++) {
					if (subject.actno != 2) break;
					if (!(subject.setMP(subject.getMP() - 3))) break;
					if (subject.checkBound(10, 0, object)) {
						object.setHP(Math.max(object.getHP() - 10, 0));
					}
					
					try {Thread.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}
				}

				subject.change(70, 70, 0);
				subject.unlock();
			}
		});
		Thread timer2 = new Thread(new Runnable() {
			public void run() {
				try {Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
				subject.unlock();
			}
		});
		timer1.start();
		timer2.start();
	}
	
	public void act(POOPet target) {
		act((POOPetKind)target);
	}
}