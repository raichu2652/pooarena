package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

import javax.swing.*;
import java.awt.*;

public class ImageLabel extends JLabel {
	public Dimension size;
	
	public ImageLabel(int width, int height, ImageIcon originalImage) {
		Image image = originalImage.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
		ImageIcon scaledImage = new ImageIcon(image);
		
		size = new Dimension(width, height);
		
		setIcon(scaledImage);
	}
}