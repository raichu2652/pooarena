package ntu.csie.oop13spring;

import javax.swing.*;

public class Mario extends POOPetKind {
	public Mario() {
		setHP(800);
		setFullHP(800);
		setRecoverHP(0);
		setMP(400);
		setFullMP(400);
		setRecoverMP(20);
		setAGI(2);
		setSize(90, 100);
		
		originalAGI = getAGI();
		
		nLabels = 3;
		charLabels = new CharLabel[nLabels];
		charLabels[0] = new CharLabel(90, 100, new ImageIcon("src\\mario\\default.png"));
		charLabels[1] = new CharLabel(140, 120, new ImageIcon("src\\mario\\firecast.png"));
		charLabels[2] = new CharLabel(90, 100, new ImageIcon("src\\mario\\psychic.png"));
		
		nEffects = 15;
		effectLabels = new EffectLabel[nEffects];
		for (int i = 0; i < 10; i++)
			effectLabels[i] = new EffectLabel(90, 90, new ImageIcon("src\\mario\\fire.png"));
		nBacks = 5;
		for (int i = 10; i < 15; i++)
		effectLabels[i] = new EffectLabel(1000, 120, new ImageIcon("src\\mario\\psychic.jpg"));

		charlabel = charLabels[0];
		System.out.println("Mario Created!");
	}
	
	public void setDefault() {
		change(90, 100, 0);
	}
	
    public POOAction act(POOArena arena) {
		POOAction action = new POOAction();

		if (skillLock) return null;

		switch(actno) {
			case 1:
				action.skill = new Firecast(this);
				action.dest = enemyPet;
				lock();
				return action;
			case 2:
				action.skill = new Psychic(this);
				action.dest = enemyPet;
				lock();
				return action;
			case 0:
			default: return null;
		}
	}
}