package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

import java.util.Scanner;
import javax.swing.*;

interface Display {
	public void showInit();
	public void showName(int ith, String name);
	public void showHP(int HP);
	public void showMP(int MP);
}

public class MyArena extends POOArena implements Display {
	enum Phase {
		START, SELECT, FIGHT, END
	}

	public MyFrame frame;
	public Phase phase;
	protected final Player Player1, Player2;
	public static POOPet[] Pets;
	protected int total;
	protected int timeInt;
	
	public MyArena() {
		Player1 = new Player();
		Player2 = new Player();

		Pets = new POOPet[5];
		
		frame = new MyFrame();
		
		Player1.panel = frame.ctrlpanel1;
		Player2.panel = frame.ctrlpanel2;
				
		phase = Phase.START;
	}
	
	public void addPet(POOPet p) {
		super.addPet(p);
		
		Pets = getAllPets();
		total++;
	}
	
	public boolean fight() {
		if (phase == Phase.START) {
			Player1.arrowLabel = new ArrowLabel(150, 150, new ImageIcon("src\\interface\\rightarrow.png"));
			Player2.arrowLabel = new ArrowLabel(150, 150, new ImageIcon("src\\interface\\leftarrow.png"));
		}
		else if (phase == Phase.SELECT) {
		}
		else if (phase == Phase.FIGHT) {
			if (Player1.Pet.getHP() == 0 || Player1.Pet.getMP() == 0) {
				try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
				
				Player1.reset();
				Player2.reset();

				for (int i = 0; i < 5; i++) {
					POOPetKind petKind = (POOPetKind)Pets[i];
					petKind.revive();
				}

				phase = Phase.END;
			}
		}
		else {
			phase = Phase.START;
		}
		
		return true;
	}
	
	public void show() {
		if (phase == Phase.START) {
			frame.showSelect(Player1, Player2);

			timeInt = 60;
			phase = Phase.SELECT;
		}
		else if (phase == Phase.SELECT) {
			if (Player1.selectPet && Player2.selectPet) {
				Player1.select(this, Pets);
				Player2.select(this, Pets);
				
				frame.showFight(Player1, Player2);
				
				Player1.Pet.setPosition(300, 300);
				Player2.Pet.setPosition(600, 300);
				
				Player1.Pet.enemyPet = Player2.Pet;
				Player2.Pet.enemyPet = Player1.Pet;

				Thread updater1 = new Thread(new Runnable() {
					public void run() {
						while (Player1.Pet != null) {
							Player1.update();
							Player1.panel.repaint();
							
							try {Thread.sleep(5);} catch (InterruptedException e) {e.printStackTrace();}
						}
					}
				});
				Thread updater2 = new Thread(new Runnable() {
					public void run() {
						while (Player2.Pet != null) {
							Player2.update();
							Player2.panel.repaint();
							
							try {Thread.sleep(5);} catch (InterruptedException e) {e.printStackTrace();}
						}
					}
				});
				
				updater1.start();
				updater2.start();
				
				timeInt = 1;
				phase = Phase.FIGHT;
			}
			else {
				Player1.update();
				Player2.update();
			}
		}
		else if (phase == Phase.FIGHT) {
		}
		else {
			frame.dispose();
			frame = new MyFrame();

			Player1.panel = frame.ctrlpanel1;
			Player2.panel = frame.ctrlpanel2;
		}
		
		try {Thread.sleep(timeInt);} catch (InterruptedException e) {e.printStackTrace();}
		return;
	}
	
	public POOCoordinate getPosition(POOPet p) {
		return null;
	}
	
	public void showInit() {}
	
	public void showName(int ith, String name) {}
	
	public void showHP(int HP) {}
	
	public void showMP(int MP) {}
}