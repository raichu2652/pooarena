package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyFrame extends JFrame {
	private final int width = 900;
	private final int height = 750;

	public GamePanel gamepanel;
	public CtrlPanel ctrlpanel1, ctrlpanel2;
	
	public MyFrame() {
		super("MyArena");
		
		setSize(width, height);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// setLayout(new FlowLayout(0, 0, 0));
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));
		
		ctrlpanel1 = new CtrlPanel();
		// ctrlpanel1.setPreferredSize(new Dimension((getWidth() - 400) / 2, getHeight()));
		ctrlpanel1.setColor(Color.red);
		add(ctrlpanel1);
		
		gamepanel = new GamePanel();
		// gamepanel.setMinimumSize(new Dimension(800, getHeight()));
		gamepanel.setPreferredSize(new Dimension(870, getHeight()));
		gamepanel.setColor(Color.blue);
		add(gamepanel);
		
		ctrlpanel2 = new CtrlPanel();
		// ctrlpanel2.setPreferredSize(new Dimension((getWidth() - 400) / 2, getHeight()));
		ctrlpanel2.setColor(Color.green);
		add(ctrlpanel2);
		
		ctrlpanel1.setPreferredSize(new Dimension((getWidth() - 400) / 2, getHeight()));
		ctrlpanel2.setPreferredSize(new Dimension((getWidth() - 400) / 2, getHeight()));
		
		pack();
		
		setVisible(true);
		setExtendedState(MAXIMIZED_BOTH);
	}
	
	public void showSelect(Player p1, Player p2) {
		gamepanel.setPetPic();

		ctrlpanel1.add(p1.arrowLabel);
		ctrlpanel2.add(p2.arrowLabel);
		addKeyListener(new SelectHandler(p1, p2));
		
		validate();
	}
	
	public void showFight(Player p1, Player p2) {
		clear();

		gamepanel.begin(p1.Pet, p2.Pet);

		ctrlpanel1.begin(p1);
		ctrlpanel2.begin(p2);
		
		addKeyListener(new ControlHandler(p1, p2));
		
		validate();
	}
	
	public void clear() {
		gamepanel.clear();
		ctrlpanel1.clear();
		ctrlpanel2.clear();
		
		KeyListener keylistener[] = getKeyListeners();
		for (int i = 0; i < keylistener.length; i++)
			removeKeyListener(keylistener[i]);
	}
}