package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
	private Color COLOR;

	public void setColor(Color c) {
		COLOR = c;
	}
	
	public void paintBorder(Graphics g) {
		g.setColor(COLOR);
		g.drawRect(0, 0, getSize().width - 1, getSize().height - 1);
	}
	
	public void clear() {
		removeAll();
		// validate();
	}
	
	public void fix() {
		setMinimumSize(getPreferredSize());
		setMaximumSize(getPreferredSize());
	}
}

class GamePanel extends MyPanel {
	public void setPetPic() {
		add(new ImageLabel(getWidth() + 20, getHeight() / 5, new ImageIcon("src\\android\\background.jpg")));
		add(new ImageLabel(getWidth() + 20, getHeight() / 5, new ImageIcon("src\\pikachu\\background.jpg")));
		add(new ImageLabel(getWidth() + 20, getHeight() / 5, new ImageIcon("src\\mario\\background.jpg")));
		add(new ImageLabel(getWidth() + 20, getHeight() / 5, new ImageIcon("src\\annivia.jpg")));
		add(new ImageLabel(getWidth() + 20, getHeight() / 5, new ImageIcon("src\\annivia.jpg")));
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

	public void begin(POOPetKind Pet1, POOPetKind Pet2) {		
		fix();

		add(Pet1.charlabel);
		Pet1.charlabel.setVisible(true);
		for (int i = 1; i < Pet1.nLabels; i++) {
			add(Pet1.charLabels[i]);
			Pet1.charLabels[i].setVisible(false);
		}
		add(Pet2.charlabel);
		Pet2.charlabel.setVisible(true);
		for (int i = 1; i < Pet2.nLabels; i++) {
			add(Pet2.charLabels[i]);
			Pet2.charLabels[i].setVisible(false);
		}

		for (int i = 0; i < Pet1.nEffects - Pet1.nBacks; i++) {
			add(Pet1.effectLabels[i]);
			Pet1.effectLabels[i].setVisible(false);
		}
		for (int i = 0; i < Pet2.nEffects - Pet1.nBacks; i++) {
			add(Pet2.effectLabels[i]);
			Pet2.effectLabels[i].setVisible(false);
		}
		
		for (int i = Pet1.nEffects - Pet1.nBacks; i < Pet1.nEffects; i++) {
			add(Pet1.effectLabels[i]);
			Pet1.effectLabels[i].setVisible(false);
		}
		for (int i = Pet2.nEffects - Pet2.nBacks; i < Pet2.nEffects; i++) {
			add(Pet2.effectLabels[i]);
			Pet2.effectLabels[i].setVisible(false);
		}
		
		setLayout(null);

		Dimension bound = getSize();
		
		Pet1.setBound(0, 0, bound.width, bound.height);
		Pet2.setBound(0, 0, bound.width, bound.height);
	}
}

class CtrlPanel extends MyPanel {
	public void begin(Player player) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createVerticalGlue());
		
		add(new HealthBar(player.Pet));
		add(new ManaBar(player.Pet));
	}
}