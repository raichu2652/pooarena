package ntu.csie.oop13spring;

import javax.swing.*;
import java.awt.*;

public abstract class POOPetKind extends POOPet {
	public int fullHP, fullMP;
	public int recoverHP, recoverMP;
	public int originalAGI;
	
	public Point point;
	public Rectangle bound;
	public Dimension size;
	
	public int actno;
	public boolean left, right, up, down;
	
	public boolean skillLock;
	
	public int nLabels;
	public CharLabel[] charLabels;
	public CharLabel charlabel;
	
	public int nEffects;
	public int nBacks;
	public EffectLabel[] effectLabels;
	
	public POOPetKind enemyPet;

	public void revive() {
		setHP(getFullHP());
		setMP(getFullMP());
		actno = 0;
		up = false;
		down = false;
		left = false;
		right = false;
		skillLock = false;
	}

	public boolean setFullHP(int fullHP) {
		if (checkHP(fullHP)) {
			this.fullHP = fullHP;
			return true;
		}
		else
			return false;
	}
	public boolean setFullMP(int fullMP) {
		if (checkMP(fullMP)) {
			this.fullMP = fullMP;
			return true;
		}
		else
			return false;
	}
	
	public int getFullHP() {
		return fullHP;
	}
	public int getFullMP() {
		return fullMP;
	}
	
	public boolean setRecoverHP(int recoverHP) {
		if (checkHP(recoverHP)) {
			this.recoverHP = recoverHP;
			return true;
		}
		else
			return false;
	}
	public boolean setRecoverMP(int recoverMP) {
		if (checkMP(recoverMP)) {
			this.recoverMP = recoverMP;
			return true;
		}
		else
			return false;
	}
	public int getRecoverHP() {
		return recoverHP;
	}
	public int getRecoverMP() {
		return recoverMP;
	}
	public void recover() {
		setHP(Math.min(getHP() + getRecoverHP(), getFullHP()));
		setMP(Math.min(getMP() + getRecoverMP(), getFullMP()));
	}
	
	public void lock() {
		skillLock = true;
	}
	public void unlock() {
		skillLock = false;
	}
	
	public void move() {
		// point = charlabel.getLocation();

		if (up) {
			point.y -= checkUp();
			charlabel.move2(point);
		}
		if (down) {
			point.y += checkDown();
			charlabel.move2(point);
		}
		if (left) {
			point.x -= checkLeft();
			charlabel.move2(point);
		}
		if (right) {
			point.x += checkRight();
			charlabel.move2(point);
		}
		if (!(up || down || left || right)) {
			charlabel.move2(point);
		}
	}
	
	public void change(int width, int height, int n) {
		setSize(width, height);
		charLabels[n].move2(point);
		
		charlabel.setVisible(false);
		charlabel.setLocation(1000, 1000);
		charlabel.setSize(0, 0);
		charlabel = charLabels[n];
		charlabel.setVisible(true);
	}
	
	public void setSize(int width, int height) {
		size = new Dimension(width, height);
	}
	
	public Dimension getSize() {
		return size;
	}
	
	public int getWidth() {
		return size.width;
	}
	
	public int getHeight() {
		return size.height;
	}
	
	public void setPosition(int x, int y) {
		point = new Point(x, y);
		
		charlabel.move2(point);
	}

	public Point getPosition() {
		return point;
	}
	
	public void setBound(int left, int top, int width, int height) {
		bound = new Rectangle(left, top, left + width, top + height);
	}
	
	public Rectangle getBound() {
		return bound;
	}
	
	public boolean checkBound(int radiusX, int radiusY, POOPetKind enemy) {
		Rectangle effectBound = new Rectangle(this.getPosition().x - radiusX, this.getPosition().y - radiusY, getWidth() + (2 * radiusX), getHeight() + (2 * radiusY));
				
		Point[] Edge = new Point[4];
		Edge[0] = new Point(enemy.getPosition());
		Edge[1] = new Point(enemy.getPosition().x, enemy.getPosition().y + enemy.getHeight());
		Edge[2] = new Point(enemy.getPosition().x + enemy.getWidth(), enemy.getPosition().y);
		Edge[3] = new Point(enemy.getPosition().x + enemy.getWidth(), enemy.getPosition().y + enemy.getHeight());

		if (effectBound.contains(Edge[0]) || effectBound.contains(Edge[1]) || effectBound.contains(Edge[2]) || effectBound.contains(Edge[3]))
			return true;
		else 
			return false;
	}
	public boolean checkBound(int radiusX, int radiusY, Rectangle bound) {
		Rectangle effectBound = new Rectangle(bound.x - radiusX, bound.y - radiusY, bound.width + 2 * radiusX, bound.height + 2 * radiusY);

		if (new Rectangle(getPosition(), getSize()).intersects(effectBound))
			return true;
		else 
			return false;
	}
	
	public int checkLeft() {
		return Math.min(point.x - bound.x, getAGI());
	}
	public int checkUp() {
		return Math.min(point.y - bound.y, getAGI());
	}
	public int checkDown() {
		return Math.min(bound.height - getHeight() - point.y, getAGI());
	}
	public int checkRight() {
		return Math.min(bound.width - getWidth() - point.x, getAGI());
	}
	
	public void setLeft(boolean newLeft) {
		left = newLeft;
	}
	public void setUp(boolean newUp) {
		up = newUp;
	}
	public void setDown(boolean newDown) {
		down = newDown;
	}
	public void setRight(boolean newRight) {
		right = newRight;
	}
	
	public void setActno(int newActno) {
		actno = newActno;
	}
	
    protected POOCoordinate move(POOArena arena) {return null;}
}