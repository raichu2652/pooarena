package ntu.csie.oop13spring;

import javax.swing.*;

public class Pikachu extends POOPetKind {
	public Pikachu() {
		setHP(300);
		setFullHP(300);
		setRecoverHP(0);
		setMP(250);
		setFullMP(250);
		setRecoverMP(4);
		setAGI(5);
		setSize(60, 70);
		
		originalAGI = getAGI();
		
		nLabels = 4;
		charLabels = new CharLabel[nLabels];
		charLabels[0] = new CharLabel(70, 70, new ImageIcon("src\\pikachu\\default.png"));
		charLabels[1] = new CharLabel(120, 60, new ImageIcon("src\\pikachu\\thunderbolt.png"));
		charLabels[2] = new CharLabel(90, 90, new ImageIcon("src\\pikachu\\charging.png"));
		charLabels[3] = new CharLabel(90, 90, new ImageIcon("src\\pikachu\\thunder.png"));
		
		nEffects = 6;
		effectLabels = new EffectLabel[nEffects];
		for (int i = 0; i < 5; i++)
			effectLabels[i] = new EffectLabel(80, 80, new ImageIcon("src\\pikachu\\bolt.png"));
		nBacks = 1;
		effectLabels[5] = new EffectLabel(1000, 1000, new ImageIcon("src\\pikachu\\thunder.jpg"));

		charlabel = charLabels[0];
		System.out.println("Pikachu Created!");
	}
	
	public void setDefault() {
		change(70, 70, 0);
	}
	
    public POOAction act(POOArena arena) {
		POOAction action = new POOAction();

		if (skillLock) return null;

		switch(actno) {
			case 1:
				action.skill = new Thunderbolt(this);
				action.dest = enemyPet;
				lock();
				return action;
			case 2:
				action.skill = new Thunder(this);
				action.dest = enemyPet;
				lock();
				return action;
			case 0:
			default: return null;
		}
	}
}