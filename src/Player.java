package ntu.csie.oop13spring;

import ntu.csie.oop13spring.*;

public class Player {
	public MyArena arena;
	public POOPetKind Pet;
	public CtrlPanel panel;
	public ArrowLabel arrowLabel;
	public int recoverDelay, recoverInterval = 30;
	public boolean selectPet;
	
	public void reset() {
		Pet = null;
		selectPet = false;
		arrowLabel.height = 0;
	}
	
	public void decide() {
		selectPet = true;
	}
	
	public void select(MyArena arena, POOPet[] pets) {
		this.arena = arena;

		Pet = (POOPetKind)pets[arrowLabel.height];

		selectPet = true;
	}
	
	public void update() {
		if (!selectPet)
			arrowLabel.move();

		if (Pet != null) {
			if (recoverDelay < recoverInterval)
				recoverDelay++;
			else {
				Pet.recover();
				recoverDelay = 0;
			}

			Pet.move();
			POOAction action = Pet.act(arena);
			if (action != null) {
				action.skill.act(action.dest);
			}
		}
	}
}