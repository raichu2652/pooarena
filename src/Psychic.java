package ntu.csie.oop13spring;

import javax.swing.*;

public class Psychic extends POOSkill{
	final POOPetKind subject;
	
	public Psychic(POOPetKind Pet) {
		subject = Pet;
	}
	
    public void act(final POOPetKind object) {
		System.out.println("Psychic!");
		
		if (!(subject.setMP(subject.getMP() - 80))) {
			subject.unlock();
			return;
		}
		
		subject.change(90, 100, 2);
		
		Thread timer1 = new Thread(new Runnable() {
			public void run() {
				try {Thread.sleep(200);} catch (InterruptedException e) {e.printStackTrace();}
				subject.change(90, 100, 0);
				subject.unlock();
				
				int n = -1;
				for (int i = 10; i < 15; i++) {
					if (!subject.effectLabels[i].enabled) {
						subject.effectLabels[i].enabled = true;
						subject.effectLabels[i].move2(0, subject.getPosition().y - 10);
						subject.effectLabels[i].setVisible(true);
						n = i;
						break;
					}
				}
				if (n < 0) return;
				
				subject.effectLabels[n].move2(0, subject.getPosition().y - 10);
				subject.effectLabels[n].setVisible(true);
				
				final int _n = n;
				for (int i = 0; i < 75; i++) {
					if (object.checkBound(1000, 0, subject.effectLabels[10].getBounds())) break;

					try {Thread.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}
				}
				if (subject.checkBound(1000, 10, object)) {
					final int agility = object.originalAGI;
					Thread timer2 = new Thread(new Runnable() {
						public void run() {
							for (int i = 0; i < 75; i++) {
								object.setAGI(1);
								
								try {Thread.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}
							}
							object.setAGI(agility);
							subject.effectLabels[_n].enabled = false;
							subject.effectLabels[_n].setVisible(false);
							subject.effectLabels[_n].move2(1000, 1000);
						}
					});
					timer2.start();
				}
				else {
					subject.effectLabels[n].enabled = false;
					subject.effectLabels[n].setVisible(false);
					subject.effectLabels[n].move2(1000, 1000);
				}
			}
		});
		timer1.start();
	}
	
	public void act(POOPet target) {
		act((POOPetKind)target);
	}
}