package ntu.csie.oop13spring;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SelectHandler extends KeyAdapter {
	Player p1, p2;
	
	public SelectHandler(Player p1, Player p2) {
		this.p1 = p1;
		this.p2 = p2;
		
		System.out.println("Select handler initialized!");
	}
	public void keyTyped(KeyEvent arg0) {}
	
	public void keyReleased(KeyEvent key) {
		switch(key.getKeyCode()) {
			case KeyEvent.VK_W:
				p1.arrowLabel.setUp(false);
				break;
			case KeyEvent.VK_S:
				p1.arrowLabel.setDown(false);
				break;
			case KeyEvent.VK_UP:
				p2.arrowLabel.setUp(false);
				break;
			case KeyEvent.VK_DOWN:
				p2.arrowLabel.setDown(false);
				break;
			case KeyEvent.VK_CONTROL:
				if (key.getKeyLocation() == KeyEvent.KEY_LOCATION_LEFT)
					p1.decide();
				else
					p2.decide();
		}
	}
	
	public void keyPressed(KeyEvent key) {
		switch(key.getKeyCode()) {
			case KeyEvent.VK_W:
				p1.arrowLabel.setUp(true);
				break;
			case KeyEvent.VK_S:
				p1.arrowLabel.setDown(true);
				break;
			case KeyEvent.VK_UP:
				p2.arrowLabel.setUp(true);
				break;
			case KeyEvent.VK_DOWN:
				p2.arrowLabel.setDown(true);
				break;
		}
	}
}