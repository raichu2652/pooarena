package ntu.csie.oop13spring;

import javax.swing.*;

public class Thunder extends POOSkill{
	final POOPetKind subject;
	
	public Thunder(POOPetKind Pet) {
		subject = Pet;
	}
	
    public void act(final POOPetKind object) {
		System.out.println("Thunder!");

		if (!(subject.setMP(subject.getMP() - 250))) {
			subject.unlock();
			return;
		}
		
		subject.change(90, 90, 2);
		subject.setAGI(0);
		
		Thread timer = new Thread(new Runnable() {
			public void run() {
				try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
				
				int HP = subject.getHP();
				boolean up = subject.up;
				boolean down = subject.down;
				boolean left = subject.left;
				boolean right = subject.right;
				
				for (int i = 0; i < 200; i++) {
					if (!(HP == subject.getHP() && up == subject.up && down == subject.down && left == subject.left && right == subject.right && subject.actno == 0)) {
						subject.change(70, 70, 0);
						subject.setAGI(7);
						subject.unlock();
						return;
					}
					
					try {Thread.sleep(10);} catch (InterruptedException e) {e.printStackTrace();}
				}
		
				subject.change(90, 90, 3);
				subject.setAGI(5);
				
				subject.effectLabels[5].move2(0, 0);
				subject.effectLabels[5].setVisible(true);
				
				object.setHP(0);
				subject.unlock();
			}
		});
		timer.start();
	}
	
	public void act(POOPet target) {
		act((POOPetKind)target);
	}
}