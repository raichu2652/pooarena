package ntu.csie.oop13spring;

import javax.swing.*;
import java.awt.*;

public class Thunderbolt extends POOSkill{
	final POOPetKind subject;
	
	public Thunderbolt(POOPetKind Pet) {
		subject = Pet;
	}
	
    public void act(final POOPetKind object) {
		System.out.println("Thunderbolt!");
		
		if (!(subject.setMP(subject.getMP() - 50))) {
			subject.unlock();
			return;
		}
		
		subject.change(120, 60, 1);
		
		Thread timer1 = new Thread(new Runnable() {
			public void run() {
				try {Thread.sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
				subject.change(70, 70, 0);
				subject.unlock();
				
				Point end = new Point(object.getPosition().x + (object.getWidth() / 2), object.getPosition().y + (object.getHeight() / 2));
				
				int n = -1;
				for (int i = 0; i < 5; i++) {
					if (!subject.effectLabels[i].enabled) {
						subject.effectLabels[i].enabled = true;
						subject.effectLabels[i].setLocation(subject.getPosition());
						subject.effectLabels[i].direct(subject.getPosition(), end, 30);
						subject.effectLabels[i].setVisible(true);
						n = i;
						break;
					}
				}
				if (n < 0) return;

				for (int i = 0; i < 200; i++) {
					if (subject.effectLabels[n].checkBound(0, 0, object)) {
						object.setHP(Math.max(object.getHP() - 120, 0));
						break;
					}
					subject.effectLabels[n].move2ward();
					
					try {Thread.sleep(10);} catch (InterruptedException e) {e.printStackTrace();}
				}
				
				subject.effectLabels[n].enabled = false;
				subject.effectLabels[n].setVisible(false);
				subject.effectLabels[n].setLocation(1000, 1000);
			}
		});
		Thread timer2 = new Thread(new Runnable() {
			public void run() {
				try {Thread.sleep(800);} catch (InterruptedException e) {e.printStackTrace();}
				subject.unlock();
			}
		});
		timer1.start();
		timer2.start();
	}
	
	public void act(POOPet target) {
		act((POOPetKind)target);
	}
}